% Instalacion de programas en GNU/Linux

<!---
pandoc instalacion_programas.md -o instalacion_programas.html -c instalacion_programas.css --toc --toc-depth 2

pandoc instalacion_programas.md -o instalacion_programas.pdf
--->


Esta guia esta pensada para distribuciones ("tipos de Linux") basadas en Ubuntu 
(por lo tanto Linux Mint, Ubuntu, Xubuntu, Kubuntu, etc.). También sirve un poco
para Debian.

Hay distintas formas de conseguir programas. Se puede usar un gestor de
paquetes, instalar un paquete manualmente, copiar el ejecutable o compilarlo vos
mismo.

Con un gestor de paquetes (recomendado):
========================================

Un gestor de paquetes termina siendo algo así como una tienda de aplicaciones,
similar a la tienda de Android (Play Store), la tienda de Apple (Apple Store),
o la nueva tienda de Windows.

Te instalan el programa, te lo agregan al menu de inicio para poder usarlo y se
encargan de actualizarlo.

El gestor de paquetes es un programa que consigue e instala los paquetes.

Un **paquete** es un archivo que tiene datos o programas. Lo importante es que
los programas pueden venir en paquetes.

Los paquetes dependen entre ellos, por eso al instalar un paquete pueden que
se descarguen varios automáticamente.

El gestor de paquetes descarga los paquetes (y por ende los programas) de
**repositorios**, que son servidores (o "páginas web") con muchos paquetes.

Uno puede elegir cuales son los repositorios que queremos usar, ver mas abajo.

Hay gestores de paquetes con interfaz gráfica y gestores que se usan desde la
terminal.

Instalar de forma grafica (mas fácil)
-------------------------------------

Hay distribuciones que están mas preparadas para usuarios principiantes (Ubuntu,
Linux Mint, etc.) que tienen un gestor de paquetes muy fácil de usar.

El nombre puede variar, suele llamarse "Centro de Software".

La forma de usarlos suele variar, pero no hacen falta muchas instrucciones
porque es cuestión de buscar el programa que quieras e instalar, como en las
tiendas de aplicaciones de móviles.

![Gestor de paquetes gráfico](imagenes/gestor_de_paquetes_grafico.jpg)

Con la terminal (más eficiente, rápido, y también es fácil):
------------------------------------------------------------

Esta es la forma que usan la mayoría de los usuarios, simplemente es cuestión de
abrir una terminal y escribir:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.bash}
sudo apt install miprogramapreferido
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Como desventaja, en este metodo tenemos que adivinar el nombre del programa, si
no sabemos el nombre exacto podemos buscar con:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.bash}
sudo apt search palabraclave
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Para desinstalar usamos:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.bash}
sudo apt remove miexprogramapreferido
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Para actualizar todos nuestros programas que instalamos con el gestor de
paquetes usamos dos comandos, ejecutamos el primero y cuando termina ejecutamos
el segundo:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.bash}
sudo apt update
sudo apt upgrade
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

###Una explicación mas extensa:

Ese comando tiene 4 palabras:

```sudo```
:   Indica que el comando debe ejecutarse como administrador

```apt```
:   Es el nombre del gestor de paquetes que usamos en las distribuciones
    derivadas de Debian, como alternativa se puede usar apt-get o aptitude (son
    prácticamente lo mismo)   

```install```
:   Es una orden que se le da a apt, queremos instalar el paquete con el nombre
    que escribiremos a continuación, como alternativa, para desinstalar usamos
    remove, para buscar, search, etc.
    
```miprogramapreferido```
:   Reemplazar por el nombre del paquete que contiene el programa a instalar.

Como puse mas arriba, podemos usar apt-get como alternativa, no hay gran
diferencia. No me gusta tanto apt-get porque si queremos buscar tenemos que
usar:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.bash}
sudo apt-cache search palabraclave
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Y no es lo mas intuitivo.

Otras distribuciones usan gestores de paquetes diferentes, por ejemplo yum o
pacman, pero su uso es muy similar a apt.


Que pasa si el programa que quiero no está?
-------------------------------------------

Los paquetes se descargan de repositorios, que son servidores que tienen los
paquetes que usamos. Si el programa que queremos no está en los repositorios que
tenemos configurados vamos a tener que agregar el repositorio necesario.

Hay repositorios que son "oficiales" o algo así como manejados por los creadores
de tu distribución, puede que tengas desactivado uno de estos repositorios, no
por qué hay que tenerlos a todos activados.

En el caso de Ubuntu y Linux Mint (Debian no) tenemos estos repositorios:

- main: Software soportado oficialmente por tu distribucion.
- universe: Software soportado por la comunidad.
- restricted: Software soportado que no esta disponible en una licencia
    completamente libre.
- multiverse: Software que no es libre.

Hay otro tipo de repositorios llamados **PPA**, que son algo así como
repositorios personales, normalmente son repositorios que solamente tienen unos
pocos programas y están generalmente mantenidos por los desarrolladores del
programa, se utilizan como alternativa cuando el programa no esta en los
repositorios anteriores.

Para saber qué repositorio necesitamos tenemos que buscar en la página web del
programa. También podemos probar a agregar los repositorios "oficiales" de
arriba si falta activar uno.

**Una vez que se haya agregado el repositorio hay que actualizar apt, para que
chequee los repositorios:**

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.bash}
sudo apt update
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

###Agregar repositorios "oficiales":

####De forma gráfica (facil):

Una forma es entrar al gestor de paquetes o "Centro de Software" y buscar alguna
opcion que permita cambiar los "Orígenes del software" o algo similar, la
ventana podría ser algo como esto:

![Selección de software](imagenes/fuentes_de_software.png)

Si está difícil encontrarlo se puede probar a abrir una terminal y probar con
este comando:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.bash}
sudo software-properties-gtk
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

O con este:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.bash}
sudo software-properties-kde
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Eso debería abrir una ventana como la de la imagen:

[Ubuntu tiene esta guía en inglés](https://help.ubuntu.com/community/Repositories/Ubuntu).

####Con la terminal (dificil):

Se debe agregar manualmente editando el archivo de texto
```/etc/apt/sources.list```

Ya es más complicado, en internet deben haber tutoriales específicos para esto.
En realidad es fácil, es solamente agregar unas líneas a ese archivo, el
problema es saber exactamente que escribir.

[La ayuda de Ubuntu tiene esta guía en inglés](https://help.ubuntu.com/community/Repositories/CommandLine).

###Agregar PPAs:

####De forma gráfica (difícil de encontrar):

Debe estar por ahí en el "Centro de Software", cerca de donde se agregan los
repositorios "oficiales".

La verdad es que hacerlo con la terminal es tan facil que solamente dejo el link
a la [ayuda de Ubuntu](https://help.ubuntu.com/community/Repositories/Ubuntu).

####Con la terminal (fácil y rápido):

Hay que ejecutar el comando:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.bash}
sudo add-apt-repository ppa:nombredelrepositorio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

O se puede agregar manualmente editando el archivo de texto
```/etc/apt/sources.list``` y agregando dos lineas:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
deb http://ppa.launchpad.net/nombredelrepositorio/ppa/ubuntu versiondedistro main 
deb-src http://ppa.launchpad.net/nombredelrepositorio/ppa/ubuntu versiondedistro main
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La ```versiondedistro``` es por ejemplo utopic, wily, saucy, etc. Si usas Linux
Mint hay que
[buscar el nombre de la version de Ubuntu en la que esta basada](http://www.linuxmint.com/oldreleases.php).
[En Ubuntu es más fácil](https://wiki.ubuntu.com/DevelopmentCodeNames).


Instalando un archivo .deb
==========================

En vez de usar un gestor de paquetes podés instalar el paquete manualmente.
Como desventaja las actualizaciones se hacen de forma manual instalando otro
.deb nuevo.

Es lo más parecido a Windows, el archivo .deb se puede pensar como un
instalador. Siempre es recomendable usar un gestor de paquetes.

Al parecer Ubuntu está buscando crear un reemplazo para los .deb, llamado
**Snappy**, de todos modos si termina implementandose se podrá seguir usando
los .deb y la transición no será rápida.

Hay más tipos de "instaladores", el otro más común es **.rpm**, pero en Debian
y derivadas (por lo tanto Ubuntu y derivadas) usamos .deb.

Instalar de forma grafica:
--------------------------

Normalmente con hacer doble click ya basta para abrir el instalador, o puede
que haya falta que hacer click derecho, "Abrir con" y seleccionar "Instalador
de paquetes", "GDebi" o algo similar.

Instalar con la terminal:
-------------------------

Hay que abrir una terminal y ejecutar el comando:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.bash}
sudo dpkg -i camino/al/instalador.deb
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Instalar copiando el ejecutable (no recomendado):
=================================================

Esto no es muy común, técnicamente no es instalar.

Es lo mismo que en Windows copiar un .exe.

Como desventaja tenemos que actualizarlo manualmente.

Una desventaja muy grande es que si queremos ver el programa en el menú de
inicio tenemos que hacer el "acceso directo" o lanzador de forma manual.

Compilando el programa (avanzado):
==================================

La ultima forma es bastante común en GNU/Linux, directamente se descarga el
código fuente, que para ser ejecutado debe ser compilado.

El **código fuente** es el programa escrito en un lenguaje entendible para las
personas, luego se tiene que compilar así se obtiene el programa en un lenguaje
entendible para la PC (ceros y unos!).

Olvidate de hacerlo de forma gráfica, hay que hacerlo desde la terminal.

El problema es que para compilar se necesitan programas que varían dependiendo
que hay que compilar. Así que antes de compilar se necesitan instalar varios
paquetes, el paquete indispensable es ```build-essentials```, contiene varios
programas dentro.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.bash}
sudo apt install build-essentials
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Después hay que instalar los demás paquetes que digan en las instrucciones,
normalmente estos paquetes terminan en -dev (viene de development, desarrollo
en ingles).

Para compilar varía de programa en programa, hay que buscar las intrucciones,
lo más común son estos tres comandos en orden:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.bash}
./configure
make
make install
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Previamente hay que moverse a la carpeta del código fuente con el comando
```cd```.

Lo malo también es que no siempre se crean los accesos directos al menú de
inicio, y para actualizar hay que compilarlo de nuevo.
