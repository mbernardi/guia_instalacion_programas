Esta es una guía básica de como instalar programas en GNU/Linux

---------------

La guía está escrita en _Markdown_ (_md_), y después genero un _HTML_ con [_Pandoc_](http://pandoc.org/]):

~~~
pandoc instalacion_programas.md -o instalacion_programas.html -c instalacion_programas.css --toc --toc-depth 2
~~~

El _CSS_ es ```instalacion_programas.css```.

También puede ser útil tener la guía en formato _PDF_, así que la generé con:

~~~
pandoc instalacion_programas.md -o instalacion_programas.pdf
~~~

---------------

Para leer la guía en _HTML_: <https://mbernardi.gitlab.io/guia_instalacion_programas/>

Para leer la guía en _PDF_: <https://mbernardi.gitlab.io/guia_instalacion_programas/instalacion_programas.pdf>
